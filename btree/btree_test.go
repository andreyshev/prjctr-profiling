package btree

import (
	"fmt"
	"math/rand"
	"runtime"
	"testing"
	"time"
)

func TestInsert(t *testing.T) {
	measuringRanges := []int{1, 100, 1000, 5000, 10000, 100000, 500000}

	resultTime := make(map[int]int64)
	resultMemAlloc := make(map[int]uint64)

	for _, r := range measuringRanges {
		btree := NewTree(10)
		var key string
		timeStart := time.Now()
		memAllocBefor := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocBefor)

		for i := 0; i < r; i++ {
			key = RandStringBytesMaskImpr(5)
			btree.Put(key, 10)
		}

		memAllocAfter := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocAfter)

		resultTime[r] = time.Now().Sub(timeStart).Nanoseconds()
		resultMemAlloc[r] = memAllocAfter.Alloc - memAllocBefor.Alloc

		for i := 0; i < 10; i++ {
			runtime.GC()
		}
	}
	for r, t := range resultTime {
		fmt.Println(r, " ", t)
	}
	time.Sleep(1 * time.Second)
	for r, t := range resultMemAlloc {
		fmt.Println(r, " ", t)
	}
}

func TestSearch(t *testing.T) {

	measuringRanges := []int{1, 100, 1000, 5000, 10000, 100000, 500000}

	resultTime := make(map[int]int64)
	resultMemAlloc := make(map[int]uint64)

	for _, r := range measuringRanges {
		btree := NewTree(10)
		var key string

		for i := 0; i < r; i++ {
			key = RandStringBytesMaskImpr(5)
			btree.Put(key, 10)
		}

		timeStart := time.Now()
		memAllocBefor := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocBefor)

		btree.Find(key)

		memAllocAfter := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocAfter)

		resultTime[r] = time.Now().Sub(timeStart).Nanoseconds()
		resultMemAlloc[r] = memAllocAfter.Alloc - memAllocBefor.Alloc

		for i := 0; i < 10; i++ {
			runtime.GC()
		}
	}
	for r, t := range resultTime {
		fmt.Println(r, " ", t)
	}
	time.Sleep(1 * time.Second)
	for r, t := range resultMemAlloc {
		fmt.Println(r, " ", t)
	}
}

func TestDelete(t *testing.T) {

	measuringRanges := []int{1, 100, 1000, 5000, 10000, 100000, 500000}

	resultTime := make(map[int]int64)
	resultMemAlloc := make(map[int]uint64)

	for _, r := range measuringRanges {
		btree := NewTree(10)
		var key string

		for i := 0; i < r; i++ {
			key = RandStringBytesMaskImpr(5)
			btree.Put(key, 10)
		}

		timeStart := time.Now()
		memAllocBefor := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocBefor)

		btree.Remove(key)

		memAllocAfter := runtime.MemStats{}
		runtime.ReadMemStats(&memAllocAfter)

		resultTime[r] = time.Now().Sub(timeStart).Nanoseconds()
		resultMemAlloc[r] = memAllocAfter.Alloc - memAllocBefor.Alloc

		for i := 0; i < 10; i++ {
			runtime.GC()
		}
	}
	for r, t := range resultTime {
		fmt.Println(r, " ", t)
	}
	time.Sleep(1 * time.Second)
	for r, t := range resultMemAlloc {
		fmt.Println(r, " ", t)
	}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImpr(n int) string {
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax letters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
